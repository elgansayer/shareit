# share_it

[![pub package](https://img.shields.io/pub/v/share_it.svg)](https://pub.dev/packages/share_it)

A plugin to share text, images and files.

The Android version uses an `androidx` library. Also remember to add the necessary permissions in your app when needed.

**Features**:
- iOS & Android support
- iPad support
- Share text, links, images and files
- Share multiple items
- Common dart interface methods & specific platform methods
- Android >= N and Android < N* support for file sharing

**TODOs**:
- Android 10 content preview support
- Check external Android storage support for file sharing

## Example

```dart
// just text (or a link)
ShareIt.text(
  content: 'Hello!!!', 
  androidSheetTitle: 'Title here!!'
);
// a link or just text if is an invalid url
ShareIt.link(
  url: 'https://www.google.com', 
  androidSheetTitle: 'Google'
);
// some file
ShareIt.file(
  path: await _imageBundlePath, 
  type: ShareItFileType.image
);
```

There are also specific platform methods if needed.

--

* Sharing of files for Android < N only seems to be working for files located in /sdcard (AFAIK).
