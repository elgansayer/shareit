library share_it;

import 'dart:io' show Platform;
import 'package:flutter/foundation.dart' show required, kReleaseMode;
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart' show MethodChannel, PlatformException;

part 'share_it_android.dart';
part 'share_it_ios.dart';
part 'utils/raw_enum.dart';
part 'utils/share_it_mixins.dart';
part 'utils/constants.dart';

// class ShareItFileType extends _RawEnum<String> {
//   const ShareItFileType(String rawValue) : super(rawValue);

//   static final image = ShareItFileType(
//       Platform.isAndroid ? "image/*" : IOSDataType.file.rawValue);

//   static final video = ShareItFileType(
//       Platform.isAndroid ? "video/*" : IOSDataType.file.rawValue);

//   static final anyFile =
//       ShareItFileType(Platform.isAndroid ? "*/*" : IOSDataType.file.rawValue);
// }

class ShareItParameters {
  final String content;
  final String path;
  final String type;
  const ShareItParameters.plainText({@required this.content})
      : path = null,
        type = null;
  const ShareItParameters({@required this.type, @required this.path})
      : content = null;
}

class ShareIt {
  static const _channel = const MethodChannel(ShareItConstants.methodChannel);

  /// You need to call `present()` to present the share sheet
  static _AndroidShareSheet android(
      {@required ShareItAndroidParameters parameters}) {
    if (kReleaseMode && !Platform.isAndroid) {
      throw PlatformException(
          code: '0',
          message: '_AndroidShareSheet is only available on Android platform');
    }
    return _AndroidShareSheet(parameters: [parameters]);
  }

  /// You need to call `present()` to present the share sheet
  static _AndroidShareSheet androidList(
      {@required List<ShareItAndroidParameters> parameters}) {
    if (kReleaseMode && !Platform.isAndroid) {
      throw PlatformException(
          code: '0',
          message: '_AndroidShareSheet is only available on Android platform');
    }
    return _AndroidShareSheet(parameters: parameters);
  }

  static Future<bool> androidText({@required String content, String title}) {
    return android(
            parameters:
                ShareItAndroidParameters.plainText(content, title: title))
        .present();
  }

  static Future<bool> androidFile(
      {@required String mimeType, @required String path, String title}) {
    return android(
            parameters: ShareItAndroidParameters.file(
                mimeType: mimeType, path: path, title: title))
        .present();
  }

  //

  /// You need to call `present()` to present the share sheet
  static _IOSActivityViewController iOS(
      {@required ShareItIOSParameters parameters}) {
    if (kReleaseMode && !Platform.isIOS) {
      throw PlatformException(
          code: '1',
          message:
              '_IOSActivityViewController is only available on iOS platform');
    }
    return _IOSActivityViewController(parameters: [parameters]);
  }

  /// You need to call `present()` to present the share sheet
  static _IOSActivityViewController iOSList(
      {@required List<ShareItIOSParameters> parameters}) {
    if (kReleaseMode && !Platform.isIOS) {
      throw PlatformException(
          code: '2',
          message:
              '_IOSActivityViewController is only available on iOS platform');
    }
    return _IOSActivityViewController(parameters: parameters);
  }

  static Future<bool> iOSText({@required String content}) {
    return iOS(parameters: ShareItIOSParameters.plainText(content)).present();
  }

  static Future<bool> iOSLink({@required String link}) {
    return iOS(parameters: ShareItIOSParameters.link(link)).present();
  }

  static Future<bool> iOSFile(
      {@required String dataType, @required String path}) {
    return iOS(
            parameters: ShareItIOSParameters.file(type: dataType, path: path))
        .present();
  }

  //

  /// - `androidSheetTitle` is ignored on iOS
  /// - if content is a valid url string, it will be converted into a `URL` object for iOS
  static Future<bool> text(
      {@required String content, String androidSheetTitle}) {
    if (Platform.isAndroid) {
      return _adaptive(
              parameters: ShareItAndroidParameters.plainText(content,
                  title: androidSheetTitle))
          .present();
    } else if (Platform.isIOS) {
      final trimmedContent = content.trim();
      return (content.startsWith("http://") ||
                  content.startsWith("https://")) &&
              trimmedContent.split("\n").length == 1
          ? _adaptive(parameters: ShareItIOSParameters.link(trimmedContent))
              .present()
          : _adaptive(parameters: ShareItIOSParameters.plainText(content))
              .present();
    }
    throw PlatformException(
        code: '3',
        message:
            'ShareIt plugin is only available on iOS and Android platforms');
  }

  static Future<bool> link({@required String url, String androidSheetTitle}) {
    assert(Uri.tryParse(url) != null);
    if (Platform.isAndroid) {
      return _adaptive(
              parameters: ShareItAndroidParameters.plainText(url.trim(),
                  title: androidSheetTitle))
          .present();
    } else if (Platform.isIOS) {
      return _adaptive(parameters: ShareItIOSParameters.link(url.trim()))
          .present();
    }
    throw PlatformException(
        code: '4',
        message:
            'ShareIt plugin is only available on iOS and Android platforms');
  }

  static Future<bool> file(
      {@required String path,
      @required String type,
      String androidSheetTitle}) {
    if (Platform.isAndroid) {
      return _adaptive(
              parameters: ShareItAndroidParameters._file(
                  path: path, rawType: type, title: androidSheetTitle))
          .present();
    } else if (Platform.isIOS) {
      return _adaptive(
              parameters: ShareItIOSParameters._file(path: path, rawType: type))
          .present();
    }
    throw PlatformException(
        code: '5',
        message:
            'ShareIt plugin is only available on iOS and Android platforms');
  }

  static Future<bool> list(
      {String androidSheetTitle,
      @required List<ShareItParameters> parameters}) {
    if (Platform.isAndroid) {
      var titleIsIncluded = false;
      return androidList(
              parameters: parameters.map((p) {
        String title;
        if (titleIsIncluded) {
          title = null;
        } else {
          title = androidSheetTitle;
          titleIsIncluded = true;
        }
        if (p.content != null) {
          return ShareItAndroidParameters.plainText(p.content, title: title);
        } else if (p.path != null) {
          return ShareItAndroidParameters._file(
              title: title, path: p.path, rawType: p.type);
        }
        return null;
      }).toList())
          .present();
    } else if (Platform.isIOS) {
      return iOSList(
              parameters: parameters.map((p) {
        if (p.content != null) {
          return ShareItIOSParameters.plainText(p.content);
        } else if (p.path != null) {
          return ShareItIOSParameters._file(path: p.path, rawType: p.type);
        }
        return null;
      }).toList())
          .present();
    }
    throw PlatformException(
        code: '6',
        message:
            'ShareIt plugin is only available on iOS and Android platforms');
  }

  // Private

  /// You need to call `present()` to present the share sheet
  static ShareItMixin _adaptive({@required ShareItParametersMixin parameters}) {
    if (Platform.isAndroid) {
      ShareItMixin output =
          android(parameters: parameters as ShareItAndroidParameters);
      return output;
    } else if (Platform.isIOS) {
      return iOS(parameters: parameters as ShareItIOSParameters);
    }

    throw PlatformException(
        code: '7',
        message:
            'ShareIt plugin is only available on iOS and Android platforms');
  }
}
