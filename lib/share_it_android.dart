part of share_it;

// class MIMEType extends _RawEnum<String> {
//   const MIMEType._(String mimeType) : super(mimeType);
//   static const plainText = const MIMEType._("text/plain");
//   static const image = const MIMEType._("image/*");
//   static const video = const MIMEType._("video/*");
//   static const anyFile = const MIMEType._("*/*");
//   static MIMEType custom(String mimeType) => MIMEType._(mimeType);
// }

class ShareItAndroidParameters implements ShareItParametersMixin {

  final String type;
  final String title;
  /// A path if is a file, or text if is raw text
  final String content;
  final bool shareAsPlainText;

  const ShareItAndroidParameters.plainText(this.content, {this.title})
      : type = "text/plain",
        shareAsPlainText = true;

  const ShareItAndroidParameters.file({this.title, @required String path, @required String mimeType})
      : this.content = path,
        this.type = mimeType,
        shareAsPlainText = false;

  ShareItAndroidParameters._file({this.title, @required String path, @required String rawType})
      : this.content = path,
        this.type = rawType,
        shareAsPlainText = false;

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'type': type,
      'content': content ?? '',
      'shareAsPlainText': shareAsPlainText
    };
  }
}

class _AndroidShareSheet implements ShareItMixin {
  final List<ShareItAndroidParameters> parameters;
  const _AndroidShareSheet({@required this.parameters});

  @override
  Future<bool> present() async {
    final result = await ShareIt._channel.invokeMethod(
      ShareItConstants.androidInvokeShareMethod,
      parameters.map((p) => p.toMap()).toList()
    );
    if (result == true) {
      return true;
    } else {
      return Future.error(result);
    }
  }
}
