part of share_it;

class _RawEnum<RawValue> extends Object {
  final RawValue rawValue;
  const _RawEnum(this.rawValue);

  @override
  String toString() => this.rawValue.toString();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is _RawEnum && this.rawValue == other.rawValue);

  @override
  int get hashCode => this.rawValue.hashCode;
}
