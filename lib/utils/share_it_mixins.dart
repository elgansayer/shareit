part of share_it;

mixin ShareItParametersMixin {
  String get type;
  String get content;
}
mixin ShareItMixin {
  List<ShareItParametersMixin> get parameters;
  Future<bool> present();
}
