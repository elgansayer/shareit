part of share_it;

class IOSDataType extends _RawEnum<String> {
  const IOSDataType._(String dataType) : super(dataType);
  static const plainText = const IOSDataType._("text");
  static const image = const IOSDataType._("image");
  static const uiImage = const IOSDataType._("uiImage");
  static const _link = const IOSDataType._("link");
  static const file = const IOSDataType._("file");
}

class ShareItIOSParameters implements ShareItParametersMixin {

  final String type;

  /// A path (URL in iOS) if is a file, or text if is raw text
  final String content;

  const ShareItIOSParameters.plainText(this.content)
      : type = "text/plain";

  const ShareItIOSParameters.link(this.content)
      : type = "IOSDataType._link";

  const ShareItIOSParameters.file({@required String path, @required this.type})
      : this.content = path;

  ShareItIOSParameters._file({@required String path, @required String rawType})
      : this.content = path,
        this.type = "";

  Map<String, dynamic> toMap() {
    return {'type': type, 'content': content ?? ''};
  }
}

class _IOSActivityViewController implements ShareItMixin {

  final List<ShareItIOSParameters> parameters;
  const _IOSActivityViewController({@required this.parameters});

  @override
  Future<bool> present() async {
    // the iOS plugin automatically returns a value in an attempt to avoid blocking the UI
    // https://github.com/flutter/flutter/issues/22024
    // that's why I use a timeout here, if something bad happens and the alert never
    // gets presented, after 10s I assume there was an error
    final result = await ShareIt._channel.invokeMethod(
      ShareItConstants.iOSInvokeShareMethod,
      parameters.map((p) => p.toMap()).toList()
    ).timeout(
      const Duration(seconds: 10),
      onTimeout: () => false
    );
    if (result == true) {
      return true;
    } else {
      return Future.error(result);
    }
  }
}
