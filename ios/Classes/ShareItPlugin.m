#import "ShareItPlugin.h"
#import <share_it/share_it-Swift.h>

@implementation ShareItPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftShareItPlugin registerWithRegistrar:registrar];
}
@end
